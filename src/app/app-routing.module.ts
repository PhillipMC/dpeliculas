import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeliculasDetallesComponent } from './peliculas/peliculas-detalles/peliculas-detalles.component';

//Componentes
import { PeliculasComponent } from './peliculas/peliculas.component';

const routes: Routes = [
  { path: '' , redirectTo: '/', pathMatch: 'full' },
  { path: '', component: PeliculasComponent},
  { path: 'p/:ID_PELICULA', component: PeliculasDetallesComponent }
  //{ path: '**', redirectTo:'/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
