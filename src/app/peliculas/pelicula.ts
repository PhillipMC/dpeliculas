export class Pelicula {
    ID_PELICULA : Number | undefined;
    TITLE: String | undefined;
    SUBTITLE: String | undefined | null;
    YEAR: String | Number | undefined;
    SUMMARY: String | undefined;
    POSTER: String | undefined;
    //POSTER_CANDIDATES: String[] | undefined;
    GENRES: String | undefined;
    RATING: Number | String | undefined;
    SIMILAR_MOVIES: String | undefined | null;
    ORIGINALLY_AVALAIBLE_AT: String | undefined;
    COUNTRIES: String | undefined;
    DATE_ADDED: String | undefined | null;
}