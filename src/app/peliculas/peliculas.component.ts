import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

//Angular
import { MatPaginator, MatPaginatorIntl, PageEvent } from '@angular/material/paginator';

import { Pelicula } from './pelicula';

import PeliculasJson from '../peliculas.json';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {

  busquedaPelicula: any;

  peliculas: Pelicula[] = [];
  currentItemsToShow: Pelicula[] = [];

  displayedColumns = ['title', 'year', 'rating'];

  dataSource = new MatTableDataSource<Pelicula>();

  @ViewChild(MatPaginator) paginator: MatPaginator = new MatPaginator(new MatPaginatorIntl(), ChangeDetectorRef.prototype);
  obs: Observable<any> = new Observable<Pelicula>();



  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.peliculas = PeliculasJson;
    this.changeDetectorRef.detectChanges();
    this.dataSource = new MatTableDataSource<any>(this.peliculas);
    this.obs = this.dataSource.connect();
    //this.currentItemsToShow = this.peliculas;
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
    //setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  errorImgNotLoading(pelicula: Pelicula) {
    console.log(pelicula.TITLE + ' ' + pelicula.YEAR);
  }

  // PARA TRABAJAR A FUTURO CON LA PAGINACION Y OTROS METODOS QUE ME PUEDEN RESULTAR UTILES

  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild('paginator') paginator!: MatPaginator;


  // ngAfterViewInit() {
  //   this.dataSource = new MatTableDataSource(this.peliculas);
  //   this.dataSource.paginator = this.paginator;
  // }

  // onPageChange($event: { pageIndex: number; pageSize: number; }) {
  //   this.currentItemsToShow = this.peliculas.slice($event.pageIndex * $event.pageSize, $event.pageIndex * $event.pageSize + $event.pageSize);
  // }


  // // MatPaginator Inputs
  // length = this.peliculas.length;
  // pageSize = 10;
  // pageSizeOptions: number[] = [5, 10, 25, 100];

  // // MatPaginator Output
  // pageEvent: PageEvent | undefined;

  // setPageSizeOptions(setPageSizeOptionsInput: string) {
  //   if (setPageSizeOptionsInput) {
  //     this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  //   }
  // }
}
