import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pelicula } from '../pelicula';
import PeliculasJson from '../../peliculas.json';


@Component({
  selector: 'app-peliculas-detalles',
  templateUrl: './peliculas-detalles.component.html',
  styleUrls: ['./peliculas-detalles.component.css']
})
export class PeliculasDetallesComponent implements OnInit {

  pelicula: Pelicula | undefined;
  id: String = '';

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('ID_PELICULA')!;
      this.pelicula = PeliculasJson.find(peliculaJson => peliculaJson.ID_PELICULA === Number(this.id));
    })
  }
}
