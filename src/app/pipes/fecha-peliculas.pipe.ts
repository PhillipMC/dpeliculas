import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fechaPeliculas'
})
export class FechaPeliculasPipe implements PipeTransform {

  transform(value: String | undefined, ...args: unknown[]): unknown {

    let dateFormated = value;
    if (value !== undefined && value.indexOf('-') > -1) {
      let splitted = value?.split("-", 3);
      console.log(splitted);
      console.log(value?.indexOf('-'));
      dateFormated = splitted![2] + '/' + splitted![1] + '/' + splitted![0];
      console.log(dateFormated);
    }
    return dateFormated;
  }
}
